class Category < ActiveRecord::Base
  
  has_many :products 
  
  scope :sorted, -> {order(:name)}
  default_scope { sorted }
  
  acts_as_nested_set
  
  validates :name, presence: true
  
  before_save :set_slug

  def descendents
    children.map do |child|
      [child] + child.descendents
    end.flatten.compact + [self]
  end

  protected

  def set_slug
    self.slug = self.name if self.slug.blank?
  end
  
end
