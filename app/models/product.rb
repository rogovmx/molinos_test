class Product < ActiveRecord::Base
  
  validates :name, :price, presence: true
  before_save :set_slug_and_img
  
  belongs_to :category
  has_many :images
  
  

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      product = find_by_id(row["id"]) || new
      product.attributes = row.to_hash.slice(*row.to_hash.keys)
      product.save!
    end
  end



  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when '.csv' then Roo::Csv.new(file.path)
    when '.xls' then Roo::Excel.new(file.path)
    when '.xlsx' then Roo::Excelx.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end


  
  
  protected

  def set_slug_and_img
    self.slug = self.name if self.slug.blank?
#    self.img = self.img.original_filename unless self.img.blank?
  end
  
end
