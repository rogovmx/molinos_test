class Image < ActiveRecord::Base
  belongs_to :product
  validates :img, presence: true
end
