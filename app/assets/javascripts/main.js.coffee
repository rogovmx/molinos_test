# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

notice = (note) ->
  $.jGrowl note if !!note
  return
window.notice = notice

nestsort = ->
  $ ->
    $("#sortable").nestedSortable
      listType: "ul"
      items: "li"
      placeholder: "highlight"
      forcePlaceholderSize: true
      handle: "div"
      helper: "clone"
      opacity: .6
      revert: 250
      tabSize: 25
      tolerance: "pointer"
      toleranceElement: "> div"

    $("#sortable").disableSelection() # make links not clickable
    $("#serialize").click ->
      csrf()
      c = set: JSON.stringify($("#sortable").nestedSortable("toHierarchy",
        startDepthCount: 0
      ))
      $.post "categories/savesort", c, $.jGrowl("Ok")        
      #false
    return
  return
window.nestsort = nestsort  
  
csrf = ->
  $(document).ajaxSend (e, xhr, options) ->
    token = $("meta[name='csrf-token']").attr("content")
    xhr.setRequestHeader "X-CSRF-Token", token
    return
  return
window.csrf = csrf    
  
$(document).ready ->
  $("title").append $("h2:first").text()
  return


$(document).ready ->
  $("#popup").draggable handle: "#popup_head"


  $("#popup_close, .popup_close").click ->
    $("#popup_content").html ""
    $("#popup").hide()
    return


  $('#products').DataTable
    lengthMenu: [[ 30, 60, 100, -1], [ 30, 60, 100, "Все"]]


