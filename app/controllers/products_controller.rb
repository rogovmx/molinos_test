class ProductsController < ApplicationController
  before_action :set_product, only: [:edit, :update, :destroy]


  def index
    @products = Product.all.includes(:images)
  end


  def new
    @product = Product.new
#    @image = Image.new
  end

  def edit
#    @images = @product.images if @product.images.any?
#    @image = Image.new
  end

  def create
    upload_image(params[:product][:img]) unless params[:product][:img].blank?
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { 
          upload_add_images params[:add_img].values, @product  unless params[:add_img].blank?
          redirect_to products_url, notice: 'Товар сохранен' 
          }
      else
        format.html { render :new }
      end
    end
  end


  def update
    respond_to do |format|
      upload_image(params[:product][:img]) unless params[:product][:img].blank?
      if @product.update(product_params)
        format.html do
          upload_add_images params[:add_img].values, @product unless params[:add_img].blank?
          redirect_to products_url, notice: 'Товар сохранен'
        end
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Товар удален' }
    end
  end
  
  def import
    Product.import(params[:file])
    redirect_to products_url, notice: 'XLS загружен'
  rescue
    redirect_to products_url, notice: 'Ошибка при загрузке XLS'
  end

  def images_upload
    #@image_name = params[:file].original_filename
    #upload_image params[:file]
  end
  
  def remove_img
    @img = Image.find(params[:id])
    @img_id = @img.id
    @img.destroy
    respond_to do |format|
      format.js { render js: "$('.img_#{@img_id}').remove()"}
    end
  end
  
  private
  
    def upload_add_images images, product
      images.each do |img|
        upload_image img
        image = Image.new(img: img.original_filename)
        product.images << image
      end
    end


   def upload_image image
     File.open(Rails.root.join('public/img', image.original_filename), 'wb') do |file|
      file.write(image.read)
    end
   end
   
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
      @images = @product.images
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params[:product][:img] = params[:product][:img].original_filename unless params[:product][:img].blank?
      params.require(:product).permit(:name, :slug, :category_id, :descr, :price, :img)
    end
end
