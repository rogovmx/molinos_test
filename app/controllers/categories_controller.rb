class CategoriesController < ApplicationController
  before_action :set_category, only: [:edit, :update, :destroy]


  def index
    @categories = Category.order(:lft)
  end


  def new
    @category = Category.new
  end

  def edit
  end


  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to categories_url }
      else
        format.html { render :new }
      end
    end
  end


  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to categories_url }
      else
        format.html { render :edit }
      end
    end
  end


  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  

  def savesort
    neworder = JSON.parse(params[:set])
    prev_item = nil
    neworder.each do |item|
      dbitem = Category.find(item['id'][2..-1])
      prev_item.nil? ? dbitem.move_to_root : dbitem.move_to_right_of(prev_item)
      sort_children(item, dbitem) unless item['children'].nil?
      prev_item = dbitem
    end
    Category.rebuild!
    @categories = Category.order(:lft)
    render 'index'
  end

  def sort_children(element,dbitem)
    prevchild = nil
    element['children'].each do |child|
      childitem = Category.find(child['id'][2..-1])
      prevchild.nil? ? childitem.move_to_child_of(dbitem) : childitem.move_to_right_of(prevchild)
      sort_children(child, childitem) unless child['children'].nil?
      prevchild = childitem
    end
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :slug, :parent_id, :lft, :rgt)
    end
end
