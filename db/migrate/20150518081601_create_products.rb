class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :slug
      t.integer :category_id
      t.text :descr
      t.integer :price
      t.string :img

      t.timestamps null: false
    end
  end
end
